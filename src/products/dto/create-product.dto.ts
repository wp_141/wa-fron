import { IsNotEmpty, Length, IsPositive, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsPositive()
  @IsNotEmpty()
  @Min(5)
  price: number;
}
